<?php
    include_once 'functions.php';
    if(isset($_POST['submit'])){
      $dbh = connect();
      $blog = addslashes($_POST["content"]);
      $sql = "INSERT INTO spost (title,content,date)
      VALUES ('".$_POST["title"]."','$blog','".$_POST["Date"]."')";
      $tag = $_POST['tag'];
      $tag = str_replace(' ', '', $tag);
      $tag = strtolower($tag);
      $words = explode(",",$tag);
      $s = sizeof($words);
      $title = $_POST ['title'];
      if ($dbh->query($sql) ) {
        for($i=0;$i<$s;$i++) {
          $tagrl = "SELECT  tag FROM tags  WHERE  tag='$words[$i]'";
          $resl = $dbh->query($tagrl);
          $resl->setFetchMode(PDO::FETCH_ASSOC);
          $count = $resl->rowCount();
          if($count==0) {
            $tagsq = "INSERT INTO tags (tag)  VALUES('$words[$i]')";
            $dbh->query($tagsq);
          }     
        // $relquery = "INSERT INTO relation(blog_id,tag_id) VALUES
        // ((SELECT idn FROM spost WHERE title='$title'),
        // (SELECT tid FROM tags WHERE tag='$words[$i]'))";
        $relquery = "INSERT INTO relation(blog_id, tag_id)
        SELECT bbid.idn, ttid.tid
        FROM spost bbid JOIN	tags ttid
        ON bbid.title = '$title' AND ttid.tag = '$words[$i]'";
        $dbh->query($relquery);
        }
        echo "<script type= 'text/javascript'>alert('New Record Inserted Successfully');</script>";
      } else {
        echo "<script type= 'text/javascript'>alert('Data not successfully Inserted.');</script>";
      }
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
          <a class="navbar-brand" href="index.php">Start Bootstrap</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="add.php">Add-blog</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

   <!-- Page Header -->
   <header class="masthead" style="background-image: url('img/a.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Add Your blog here</h1>
            <span class="subheading">Introvert?.Then this is your chance</span>
          </div>
        </div>
      </div>
    </div>
  </header>     

   <!-- Main Content -->
   <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <p>Blogging is a communications mechanism handed to us by the long tail of the Internet</p>
             <form action="" method="POST">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label>Title</label>
                  <input type="text" class="form-control" placeholder="Title" name="title" required data-validation-required-message="Please enter your blog Titile.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Tags</label>
                  <input type="tel" class="form-control" placeholder="Tags" name="tag" id="tag" required data-validation-required-message="Please enter your phone number.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label>Content</label>
                  <textarea rows="5" class="form-control" placeholder="Content" name="content" required data-validation-required-message="Please enter Your content. Atleast 1000 words."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                    <div class="form-group col-xs-12 floating-label-form-group controls">
                      <label>Date of publish</label>
                      <input type="date" id="date" name="Date" 
                      value="2019-01-01"
                      min="2019-01-01" max="2019-12-31" required data-validation-required-message="Please set the date">
                      <p class="help-block text-danger"></p>
                    </div>
                  </div>
              <br>
              <div id="success"></div>
              <div class="form-group">
              <input type="submit" value=" Submit " name="submit"/>
              </div>
            </form>
          </div>
        </div>
      </div>
      <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="js/jqBootstrapValidation.js"></script>
  <script src="js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>